'''
Utility to convert wav files to mp3 with processing options: fade out, truncate beginning/end silence, bitrate, normalize decibels. 
Requires avlib installed on OS to run.
'''
import os
import pydub


class WavtoMp3:
    def __init__(self, wavpath, mp3path=None, truncate_silences=True, bitrate='256k', normalize=1, fade=2000):
        ''' 
        required:
            wavpath - specify input wave path 

        optional:
            mp3path - mp3 output name. if no mp3 path is given, it will use the wav file name with an mp3 extension
            truncate_silences = True to remove silences from beginning and end of track, False to skip
            bitrate - default is 256k
            normalize - specify decibels (False to skip)
            fade - track fadeout time in milliseconds (False to skip)
        '''

        self.sound = pydub.AudioSegment.from_wav(wavpath)
        if normalize:
            self._normalize(normalize)
        if truncate_silences:
            self._rm_silence()

        if fade:
            self._fade(fade)

        self._export(wavpath, mp3path, bitrate)

    def detect_end_silence(sound, silence_threshold=-50.0, chunk_size=10):

        trim_ms = len(sound)-1  # ms
        assert chunk_size > 0  # to avoid infinite loop
        while sound[trim_ms:trim_ms+chunk_size].dBFS < silence_threshold and trim_ms > len(sound):
            trim_ms -= chunk_size
        return trim_ms

    def _normalize(self, hr):
        self.sound = pydub.effects.normalize(self.sound, headroom=hr)
        print('normalized audio')

    def _rm_silence(self):
        sound = self.sound
        start_silence = pydub.silence.detect_leading_silence(sound)
        sound = sound[start_silence:]
        end_silence = WavtoMp3.detect_end_silence(sound)
        sound = sound[:end_silence]
        self.sound = sound
        print('removed intro + end silence')

    def _fade(self, fade):
        self.sound = self.sound.fade_out(3000)
        print('added fade out')

    def _export(self, wavpath, mp3path, bitrate):
        if mp3path is None:
            mp3path = wavpath.replace('.wav', '.mp3').replace('.WAV', '.mp3')
        if os.path.exists(mp3path):
            os.remove(mp3path)
            assert not os.path.exists(mp3path), 'old file not deleted'
            print('deleted old file!')

        self.sound.export(mp3path, format="mp3", bitrate=str(bitrate))
        print(f'exported {mp3path}')


if __name__ == "__main__":

    WavtoMp3(wavpath='ratm.wav')
